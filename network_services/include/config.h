#ifndef CONFIG_H_
#define CONFIG_H_

#include <boost/thread/mutex.hpp>
#include <boost/thread/locks.hpp>
#include <boost/thread/condition.hpp>
#include <stdint.h>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <boost/asio/ip/tcp.hpp>


typedef uint64_t t_data; ///< тип данных для обмена между процессами

typedef boost::mutex::scoped_lock t_scoped_lock;
typedef boost::mutex t_mutex;
typedef boost::condition t_condition;

typedef boost::shared_ptr<boost::asio::ip::tcp::socket> socket_ptr;

const extern char CR;

extern const char *FLOW_INIT_TO_ECHO;
extern const char *FLOW_ECHO_TO_INIT;

extern const char *FLOW_WRITER;
extern const char *FLOW_READER;

#endif // CONFIG_H_
