#ifndef AUTO_VALUE_H
#define	AUTO_VALUE_H

#include <stdint.h>

/** addgroup AUTO_VALUE автоматическая инициализация простых типов
 * @see http://www.rsdn.ru/forum/src/174720.flat.aspx  тема - SRC:auto_value<>
 */

template< class T, T i = 0 >
class auto_value
{
	T t_;
public:
	typedef T data_t;
	typedef auto_value& self_t;

	// конструктор по умолчанию - главное достоинство этой тулзы
	inline auto_value() : t_(i) {}

	// конструктор с 1 параметром (в том числе - конструктор копирования)
	template< class V >
	inline auto_value(V v) : t_(v) {}

	// доступ к данному
	inline const T& data() const { return t_; }
	inline T& data() { return t_; }

	// считается, что исходный тип - простой
	operator T () const { return t_; }

	// операторы присваивания
	template< class V > inline self_t operator =   (V v) { t_ =   v; return *this; }
	template< class V > inline self_t operator +=  (V v) { t_ +=  v; return *this; }
	template< class V > inline self_t operator -=  (V v) { t_ -=  v; return *this; }
	template< class V > inline self_t operator *=  (V v) { t_ *=  v; return *this; }
	template< class V > inline self_t operator /=  (V v) { t_ /=  v; return *this; }
	template< class V > inline self_t operator %=  (V v) { t_ %=  v; return *this; }
	template< class V > inline self_t operator &=  (V v) { t_ &=  v; return *this; }
	template< class V > inline self_t operator |=  (V v) { t_ |=  v; return *this; }
	template< class V > inline self_t operator ^=  (V v) { t_ ^=  v; return *this; }
	template< class V > inline self_t operator <<= (V v) { t_ <<= v; return *this; }
	template< class V > inline self_t operator >>= (V v) { t_ >>= v; return *this; }
	inline self_t operator ++ ()    { ++t_; return *this; } // префиксный
	inline data_t operator ++ (int) { return t_++; } // постфиксный
	inline self_t operator -- ()    { --t_; return *this; } // префиксный
	inline data_t operator -- (int) { return t_--; } // постфиксный
	inline bool operator==(const self_t o) { return o.t_ == t_; }
};

class DOUBLE
{
	typedef double T;
	T t_;
public:
	typedef T data_t;
	typedef DOUBLE& self_t;

	// конструктор по умолчанию - главное достоинство этой тулзы
	inline DOUBLE() : t_(T()) {}

	// конструктор с 1 параметром (в том числе - конструктор копирования)
	template< class V >
	inline DOUBLE(V v) : t_(v) {}

	// доступ к данному
	inline const T& data() const { return t_; }
	inline T& data() { return t_; }

	// считается, что исходный тип - простой
	operator T () const { return t_; }

	// операторы присваивания
	template< class V > inline self_t operator =   (V v) { t_ =   v; return *this; }
	template< class V > inline self_t operator +=  (V v) { t_ +=  v; return *this; }
	template< class V > inline self_t operator -=  (V v) { t_ -=  v; return *this; }
	template< class V > inline self_t operator *=  (V v) { t_ *=  v; return *this; }
	template< class V > inline self_t operator /=  (V v) { t_ /=  v; return *this; }
	template< class V > inline self_t operator %=  (V v) { t_ %=  v; return *this; }
	template< class V > inline self_t operator &=  (V v) { t_ &=  v; return *this; }
	template< class V > inline self_t operator |=  (V v) { t_ |=  v; return *this; }
	template< class V > inline self_t operator ^=  (V v) { t_ ^=  v; return *this; }
	template< class V > inline self_t operator <<= (V v) { t_ <<= v; return *this; }
	template< class V > inline self_t operator >>= (V v) { t_ >>= v; return *this; }
	inline self_t operator ++ ()    { ++t_; return *this; } // префиксный
	inline data_t operator ++ (int) { return t_++; } // постфиксный
	inline self_t operator -- ()    { --t_; return *this; } // префиксный
	inline data_t operator -- (int) { return t_--; } // постфиксный
	inline bool operator==(const self_t o) { return o.t_ == t_; }
};


/** @} */

/** @{
 * Простые типы, автоматически инициализируемые нулём
 */
typedef auto_value<bool, false> BOOL;
typedef auto_value<uint8_t, 0> UINT8_T;
typedef auto_value<uint16_t, 0> UINT16_T;
typedef auto_value<uint32_t, 0> UINT32_T;
typedef auto_value<uint64_t, 0> UINT64_T;

typedef auto_value<int8_t, 0> INT8_T;
typedef auto_value<int16_t, 0> INT16_T;
typedef auto_value<int32_t, 0> INT32_T;
typedef auto_value<int64_t, 0> INT64_T;

typedef auto_value<size_t, 0> SIZE_T;
typedef auto_value<ssize_t, 0> SSIZE_T;

/** @} */

#endif	/* AUTO_VALUE_H */

