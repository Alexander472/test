#ifndef STDCONT_PRINT_H
#define STDCONT_PRINT_H

#include <vector>
#include <set>
#include <iterator>
#include <boost/shared_ptr.hpp>

template <typename CONT>
std::ostream& print_container(std::ostream &s, const CONT &cont)
{
    for (typename CONT::const_iterator it = cont.begin(), end = cont.end(); it != end; ++it)
    {
        s << "(" << *it << ")";
    }
    return s;
}

namespace std
{

///@WORKAROUND http://www.mail-archive.com/gcc-bugs@gcc.gnu.org/msg151173.html
template<typename T>
std::ostream& operator<<(std::ostream& s, const std::vector<T>& cont)
{
    return print_container(s, cont);
}

template<typename T>
std::ostream& operator<<(std::ostream& s, const std::set<T>& cont)
{
    return print_container(s, cont);
}

template<typename T>
std::ostream& operator<<(std::ostream& s, const std::vector<boost::shared_ptr<T> >& cont)
{
    for (typename std::vector<boost::shared_ptr<T> >::const_iterator it = cont.begin(), end = cont.end(); it != end; ++it)
    {
        s << "(";
        s << it->get();
        if (*it)
        {
            s << "->[" << **it << "]";
        }
        s << ")";
    }
    return s;
}

}

#endif // STDCONT_PRINT_H
