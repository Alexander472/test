#include "Handshake.h"
#include "common/mylog.h"
#include <boost/asio.hpp>

using namespace std;

Handshake::Handshake(socket_ptr sock, const std::string &queue_name, const std::string &type)
    : m_sock(sock)
{
    ostringstream s;
    s << queue_name << " " << type;

    Write(s.str());

    string reply = Read();

    if ("ACK" != reply)
    {
        assert(0);
    }
}

void Handshake::Write(const std::string &s)
{
    boost::asio::streambuf m_data;

    ostream m_os(&m_data);
    m_os << s << CR;

    boost::asio::write(*m_sock, m_data);

    SDBG("W:" << s);
}

std::string Handshake::Read()
{
    boost::asio::streambuf m_data;

    boost::asio::read_until(*m_sock, m_data, CR);

    std::string val;

    std::istream m_is(&m_data);
    m_is >> val;

    SDBG("R:" << val);

    return val;
}
