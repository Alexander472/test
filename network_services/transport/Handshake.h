#ifndef HANDSHAKE_H_
#define HANDSHAKE_H_

#include "config.h"

class Handshake
{
public:

    Handshake(socket_ptr sock, const std::string &queue_name, const std::string &type);
    void Write(const std::string &s);
    std::string Read();

private:
    socket_ptr m_sock;
};

#endif /* HANDSHAKE_H_ */
