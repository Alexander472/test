#include "control/Pipe.h"
#include "control/ProxyFlow.h"
#include "control/Controller.h"
#include "readers/generator.h"
#include "readers/SocketReader.h"
#include "readers/ProxyFlowReader.h"
#include "writers/ProxyFlowWriter.h"
#include "writers/StreamWriter.h"
#include "writers/SocketWriter.h"
#include "transport/Handshake.h"

#include <fstream>
#include <boost/thread.hpp>
#include <boost/lexical_cast.hpp>

using namespace std;
using namespace boost::asio;

io_service my_io_service;

int main(int argc, char *argv[])
{
    if (argc != 5)
    {
        cerr << "Usage: echo_server <proxy address> <proxy port> <sending threads count> <recieving threads count>" << endl;
        cerr << endl;
        cerr << "Ex.: echo_server 127.0.0.1 5000 2 3" << endl;
        cerr << " - connect to proxy 127.0.0.1 to port 5000 and use 2 sending and 3 recieving threads" << endl;
        return EXIT_FAILURE;
    }

    const size_t writers_count = boost::lexical_cast<size_t>(argv[3]);
    const size_t readers_count = boost::lexical_cast<size_t>(argv[4]);

    Controller controller("EchoServer", boost::make_shared<std::istream>(cin.rdbuf()));

    ProxyFlow<t_data>::pointer output_file_flow = ProxyFlow<t_data>::Create("echo_receive.txt");
    ProxyFlow<t_data>::pointer send_to_init_server_flow = ProxyFlow<t_data>::Create("send_to_init");

    ip::tcp::resolver resolver(my_io_service);
    ip::tcp::resolver::query query(argv[1], argv[2]);

    /* create readers from proxy for InitToEcho queue */
    for (size_t i = readers_count; i > 0; --i)
    {
        socket_ptr socket(new ip::tcp::socket(my_io_service));
        boost::asio::connect(*socket, resolver.resolve(query));
        Handshake(socket, FLOW_INIT_TO_ECHO, FLOW_READER);

        controller.AddPipe(Pipe::Create(SocketReader::Create(socket),
                {
                    ProxyFlowWriter::Create(output_file_flow),
                    ProxyFlowWriter::Create(send_to_init_server_flow)
                }));
    }

    /* create writers to proxy for EchoToInit queue*/
    for (size_t i = writers_count; i > 0; --i)
    {
        socket_ptr socket(new ip::tcp::socket(my_io_service));
        boost::asio::connect(*socket, resolver.resolve(query));
        Handshake(socket, FLOW_ECHO_TO_INIT, FLOW_WRITER);

        controller.AddPipe(Pipe::Create(ProxyFlowReader::Create(send_to_init_server_flow),
                SocketWriter::Create(socket)));
    }

    controller.AddPipe(Pipe::Create(ProxyFlowReader::Create(output_file_flow), StreamWriter::Create(boost::make_shared<std::ofstream>("echo_recieve.txt"))));

    controller.Run();

    return EXIT_SUCCESS;
}
