#ifndef PROXYFLOWWRITER_H_
#define PROXYFLOWWRITER_H_

#include "writers/writer.h"
#include "control/ProxyFlow.h"

class ProxyFlowWriter: public IWriter
{
public:
    ProxyFlowWriter(ProxyFlow<t_data>::pointer proxyFlow) :
        IWriter(),
        m_pipeId(proxyFlow->Register()),
        m_proxyFlow(proxyFlow)
    {}

    virtual void Write(const t_data &val)
    {
        m_proxyFlow->Push(m_pipeId, val);
    }

    static IWriter::pointer Create(ProxyFlow<t_data>::pointer proxyFlow)
    {
        return boost::make_shared<ProxyFlowWriter>(proxyFlow);
    }

private:
    t_pipe_id m_pipeId;
    ProxyFlow<t_data>::pointer m_proxyFlow;
};

#endif /* PROXYFLOWWRITER_H_ */
