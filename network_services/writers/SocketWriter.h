#ifndef SOCKETWRITER_H_
#define SOCKETWRITER_H_

#include <boost/asio.hpp>
#include "writers/writer.h"
#include "common/mylog.h"

class SocketWriter: public IWriter
{
public:
    SocketWriter(socket_ptr sock) :
        IWriter(),
        m_sock(sock)
    {}

    virtual void Write(const t_data &val)
    {
        boost::asio::streambuf m_data;
        std::ostream m_os(&m_data);
        m_os << val << CR;

        boost::asio::write(*m_sock, m_data);

        SDBG("W:" << val);
    }

    static boost::shared_ptr<SocketWriter> Create(socket_ptr sock)
    {
        return boost::shared_ptr<SocketWriter>(new SocketWriter(sock));
    }

private:
    socket_ptr m_sock;
};

#endif /* SOCKETWRITER_H_ */
