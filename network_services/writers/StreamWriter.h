#ifndef STREAMWRITER_H_
#define STREAMWRITER_H_

#include "writers/writer.h"
#include <boost/make_shared.hpp>
#include <iostream>

class StreamWriter : public IWriter
{
public:
    StreamWriter(boost::shared_ptr<std::ostream> s) : IWriter(),
            m_stream(s)
    {}

    virtual void Write(const t_data &val)
    {
        *m_stream << val << std::endl;
    }

    static boost::shared_ptr<StreamWriter> Create(boost::shared_ptr<std::ostream> s)
    {
        return boost::make_shared<StreamWriter>(s);
    }

private:
    boost::shared_ptr<std::ostream> m_stream;
};

#endif /* STREAMWRITER_H_ */
