#ifndef WRITER_H_
#define WRITER_H_

#include "config.h"

#include <memory>

class IWriter
{
public:
    typedef boost::shared_ptr<IWriter> pointer;

    virtual ~IWriter() {};

    virtual void Write(const t_data &val) = 0;
};


typedef std::vector<IWriter::pointer> t_writers_cont;

#endif // WRITER_H_
