#ifndef CONSOLEWRITER_H_
#define CONSOLEWRITER_H_

#include "config.h"
#include "writers/writer.h"

class ConsoleWriter : public IWriter
{
public:
    ConsoleWriter() : IWriter(),
            m_id(ids++)
    {}

    virtual void Write(const t_data &val)
    {
        t_scoped_lock lk(mutex_cout);

        std::cout << m_id << " : " << val << std::endl;
    }

    static boost::shared_ptr<ConsoleWriter> Create()
    {
        return boost::shared_ptr<ConsoleWriter>(new ConsoleWriter());
    }

private:
    size_t m_id;

    static size_t ids;
    static t_mutex mutex_cout;
};

#endif /* CONSOLEWRITER_H_ */
