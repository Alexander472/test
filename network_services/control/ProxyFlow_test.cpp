#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include "ProxyFlow.h"

#include <iostream>
#include <algorithm>

#include "print_cont.h"

using namespace std;

typedef boost::shared_ptr<ProxyFlow<t_data> > t_proxy_flow_test;

/* helper for testing private function ProxyFlow::IsEmpty */
void test_proxy_flow()
{
    t_proxy_flow_test pr = make_shared<ProxyFlow<t_data> >("test", make_shared<Limiter<t_data> >());

    REQUIRE(true == pr->IsEmpty());

    t_pipe_id id1 = pr->Register();
    t_pipe_id id2 = pr->Register();
    t_pipe_id id3 = pr->Register();


    pr->Push(id1, 103);
    pr->Push(id2, 101);
    pr->Push(id3, 102);

    REQUIRE(false == pr->IsEmpty());

    REQUIRE(101 == pr->Pop());
    pr->Unregister(id2);
    REQUIRE(102 == pr->Pop());
    pr->Unregister(id3);
    REQUIRE(103 == pr->Pop());
    pr->Unregister(id1);

    REQUIRE(true == pr->IsEmpty());
}

TEST_CASE("ProxyFlow/StepNext", "")
{
    test_proxy_flow();
}
