#ifndef LIMITER_H_
#define LIMITER_H_

#include "auto_value.h"
#include "base_error.h"
#include "config.h"
#include "print_cont.h"
#include "common/mylog.h"

#include <map>
#include <set>
#include <memory>
#include <functional>

typedef uint32_t t_pipe_id;

#if 0

class IData
{
    IData() {}

    void Write(std::ostream &os)
    {
        doWrite(os);
    }
    void Read(std::istream &is)
    {
        doRead(is);
    }
private:
    virtual doWrite(std::ostream &os) = 0;
    virtual doRead(std::istream &is) = 0;
};

class Uint64Data : public IData
{
    doWrite(std::ostream)
    {
        os <<
    }
};

#endif

template <typename T, typename CMP = std::less<typename T::second_type> >
struct PairCompareSecond : public std::binary_function<T, T, bool>
{
    bool operator()(const T& left, const T& right) const
    {
        return left.second < right.second;
    }
};

template<typename T>
class Limiter
{
public:
    typedef boost::shared_ptr<Limiter> pointer;

    Limiter() :
        it_limit(m_limits.end())
    {}

    t_pipe_id Register()
    {
        const t_pipe_id id = ++m_registerCount;

        BlockerAdd(id);

        return id;
    }

    void Unregister(t_pipe_id pipeId)
    {
        m_limits.erase(pipeId);

        BlockerDelete(pipeId);

        InvalidateCached();
    }

    void Push(t_pipe_id pipeId, const T &val)
    {
        BlockerDelete(pipeId);

        m_limits[pipeId] = val;

        InvalidateCached();
    }

    bool CanSend(const T &val)
    {
        if (!m_blockers.empty())
        {
            SDBG("BLOCKS : " << m_blockers);

            return false;
        }

        if (m_limits.empty())
        {
            return true;
        }

        return val <= GetLimit();
    }

private:

    void BlockerAdd(t_pipe_id id)
    {
        SDBG("BLOCK ADD : " << id);
        m_blockers.insert(id);
    }
    void BlockerDelete(t_pipe_id id)
    {
        //SDBG("BLOCK DEL : " << id);
        m_blockers.erase(id);
    }

    T GetLimit()
    {

        SOFT_CHECK(!m_limits.empty());
        SOFT_CHECK(m_blockers.empty());

        if (it_limit == m_limits.end())
        {
            it_limit = std::min_element(m_limits.begin(), m_limits.end(), PairCompareSecond<typename cont_limits::iterator::value_type>());
        }

        return it_limit->second;
    }

    void InvalidateCached()
    {
        it_limit = m_limits.end();
    }

    typedef std::map<t_pipe_id, T> cont_limits;

    cont_limits m_limits;
    typename cont_limits::const_iterator it_limit;

    SIZE_T m_registerCount;

    std::set<t_pipe_id> m_blockers;
};

#endif /* LIMITER_H_ */
