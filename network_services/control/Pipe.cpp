#include "Pipe.h"
#include "common/mylog.h"

static t_mutex pause_mutex;
static t_condition program_wait_for_continue;

static bool program_paused = true;

void pause_program()
{
    t_scoped_lock lk(pause_mutex);

    program_paused = true;
}

void continue_program()
{
    t_scoped_lock lk(pause_mutex);

    program_paused = false;

    program_wait_for_continue.notify_all();
}

static bool is_paused()
{
    return program_paused;
}

PipeRunner::PipeRunner(Pipe::pointer pipe) :
    m_pipe(pipe)
{}

void PipeRunner::operator()()
{
    try
    {
        SINFO("Started");

        while(m_pipe->ProcessOne())
        {
            boost::this_thread::interruption_point();

            if (is_paused())
            {
                t_scoped_lock lk(pause_mutex);

                while (is_paused())
                {
                    SDBG("PAUSED");
                    program_wait_for_continue.wait(lk);
                }
            }
        }
    }
    catch(const boost::system::system_error &e)
    {
        // process all other errors;
    }

    SINFO("Exited");
}

