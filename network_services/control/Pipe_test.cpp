
#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include "readers/reader.h"
#include "writers/writer.h"
#include "control/Pipe.h"

#include <vector>
#include <deque>
#include <memory>

using namespace std;

class ReaderMock : public IReader
{
public:
    ReaderMock(std::deque<t_data> vals) : IReader(),
        m_vals(vals)
    {}

    virtual bool Read(t_data &val)
    {
        if (m_vals.empty())
        {
            return false;
        }
        val = m_vals.front();
        m_vals.pop_front();
        return true;
    }
    
    static boost::shared_ptr<ReaderMock> Create(const std::deque<t_data> &init)
    {
        return boost::make_shared<ReaderMock>(init);
    }
    
    deque<t_data> m_vals;
};

typedef boost::shared_ptr<ReaderMock> t_reader_mock;

class WriterMock : public IWriter
{
public:
    WriterMock(const vector<t_data> &vals) : IWriter(),
        m_vals(vals)
    {}

    virtual void Write(const t_data &val)
    {
        m_vals.push_back(val);
    }

    static boost::shared_ptr<WriterMock> Create(const vector<t_data> &init)
    {
        return boost::make_shared<WriterMock>(init);
    }
    
    vector<t_data> m_vals;
};

typedef boost::shared_ptr<WriterMock> t_writer_mock;

TEST_CASE("pipe/PushWriters", "all writers pushed")
{
    t_reader_mock r = ReaderMock::Create({1, 2, 3});
    t_writer_mock w1 = WriterMock::Create({});
    t_writer_mock w2 = WriterMock::Create({});

    Pipe::pointer pipe = Pipe::Create(r, {w1, w2});

    pipe->ProcessOne();
    pipe->ProcessOne();
    pipe->ProcessOne();

    REQUIRE(deque<t_data>({}) == r->m_vals);
    REQUIRE(vector<t_data>({1,2,3}) == w1->m_vals);
    REQUIRE(vector<t_data>({1,2,3}) == w2->m_vals);
}
