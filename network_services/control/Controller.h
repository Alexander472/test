#ifndef CONTROLLER_H_
#define CONTROLLER_H_

#include "config.h"
#include "control/Pipe.h"

#include <boost/shared_ptr.hpp>
#include <boost/thread/thread.hpp>

/** User commands processor */
class Controller
{
public:
    typedef boost::shared_ptr<boost::thread> thread_ptr;

    Controller(const std::string &controllerName, boost::shared_ptr<std::istream> controllFlow);

    /** Run until exit */
    void Run();

    /**
     * Add pusher for processing
     * @param pipe added pipe
     */
    void AddPipe(Pipe::pointer pipe);


    /**
     * Set processed User Commands subscriber
     * @param subsriber
     */
    void SetCommandSubscriber(IWriter::pointer subscriber);

private:
    bool Start(); ///< start processing
    bool Stop(); ///< Stop processing
    bool Exit(); ///< exit processing cycle

private:
    std::string m_controllerName;
    IWriter::pointer m_commandsSubcriber;

    boost::shared_ptr<std::istream> m_controllFlow; ///< user commands input stream

//    std::vector<thread_ptr> m_threads; ///< running threads
    boost::thread_group m_threads;

    std::vector<Pipe::pointer> m_pipes; ///< pipes for processing in a thread
};

#endif /* CONTROLLER_H_ */
