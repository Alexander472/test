#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include "Limiter.h"

TEST_CASE("Limiter/Empty", "empty limiter allows data")
{
    Limiter<t_data> l;

    REQUIRE(true == l.CanSend(100));
}

TEST_CASE("Limiter/Blocker", "blocker will prevent data")
{
    Limiter<t_data> l;

    const t_pipe_id id1 = l.Register();
    const t_pipe_id id2 = l.Register();

    REQUIRE(false == l.CanSend(100));

    l.Unregister(id1);

    REQUIRE(false == l.CanSend(100));

    l.Unregister(id2);

    REQUIRE(true == l.CanSend(100));
}

TEST_CASE("Limiter/Push1", "check one pusher")
{
    Limiter<t_data> l;

    const t_pipe_id id1 = l.Register();

    l.Push(id1, 100);

    REQUIRE(true == l.CanSend(10));
    REQUIRE(true == l.CanSend(100));
    REQUIRE(false == l.CanSend(101));
}

TEST_CASE("Limiter/Push2", "check pusher replace limit")
{
    Limiter<t_data> l;

    const t_pipe_id id1 = l.Register();

    l.Push(id1, 100);
    l.Push(id1, 200);

    REQUIRE(true == l.CanSend(101));
    REQUIRE(true == l.CanSend(200));
    REQUIRE(false == l.CanSend(201));
}


TEST_CASE("Limiter/Push3", "check pusher replace limit")
{
    Limiter<t_data> l;

    const t_pipe_id id1 = l.Register();
    const t_pipe_id id2 = l.Register();
    const t_pipe_id id3 = l.Register();

    l.Push(id1, 100);
    REQUIRE(false == l.CanSend(100));

    l.Push(id2, 200);
    REQUIRE(false == l.CanSend(100));

    l.Push(id3, 300);
    REQUIRE(true == l.CanSend(100));
    REQUIRE(false == l.CanSend(200));

    l.Push(id1, 400);
    REQUIRE(true == l.CanSend(200));
    REQUIRE(false == l.CanSend(300));

    l.Push(id2, 500);
    REQUIRE(true == l.CanSend(300));
    REQUIRE(false == l.CanSend(400));

    l.Push(id2, 1000);
    REQUIRE(true == l.CanSend(300));
    REQUIRE(false == l.CanSend(400));

    l.Push(id3, 600);
    REQUIRE(true == l.CanSend(300));
    REQUIRE(true == l.CanSend(400));
    REQUIRE(false == l.CanSend(500));
}

TEST_CASE("Limiter/Unregister", "check unregistered pusher stops limiting")
{
    Limiter<t_data> l;

    const t_pipe_id id1 = l.Register();
    const t_pipe_id id2 = l.Register();

    l.Push(id1, 100);
    l.Push(id2, 200);

    REQUIRE(true == l.CanSend(100));
    REQUIRE(false == l.CanSend(200));
    REQUIRE(false == l.CanSend(300));

    l.Unregister(id1);

    REQUIRE(true == l.CanSend(200));
    REQUIRE(false == l.CanSend(300));

    l.Unregister(id2);

    REQUIRE(true == l.CanSend(300));
}
