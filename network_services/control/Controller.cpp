#include "Controller.h"
#include "common/mylog.h"

#include <boost/thread/thread.hpp>

using namespace std;

Controller::Controller(const std::string &controllerName, boost::shared_ptr<std::istream> controllFlow) :
    m_controllerName(controllerName),
    m_controllFlow(controllFlow)
{}

void Controller::AddPipe(Pipe::pointer pipe)
{
    m_pipes.push_back(pipe);
}

void Controller::Run()
{
    struct
    {
        string name;
        bool (Controller::*func)();
    }
    static const data[] =
    {
            { "start", &Controller::Start },
            { "stop", &Controller::Stop },
            { "exit", &Controller::Exit },
    };

    string s;

    while (getline(*m_controllFlow, s))
    {
        for (auto i : data)
        {
            if (s == i.name)
            {
                if (true == (this->*i.func)())
                {
                    return;
                }
            }
        }
    }
}

bool Controller::Start()
{
    for (auto pipe : m_pipes)
    {
        m_threads.create_thread(PipeRunner(pipe));
    }

    m_pipes.clear();

    continue_program();

    return false;
}

bool Controller::Stop()
{
    pause_program();

    return false;
}

void Controller::SetCommandSubscriber(IWriter::pointer subscriber)
{
    m_commandsSubcriber = subscriber;
}

bool Controller::Exit()
{
    m_threads.interrupt_all();

    continue_program();

    SDBG("Join all");

    m_threads.join_all();

    SDBG("Join done");

    return true; // signal exit with 'true'
}
