#ifndef PROXYFLOW_H_
#define PROXYFLOW_H_

#include "config.h"
#include "Limiter.h"

#include <queue>
#include <memory>
#include <algorithm>
#include <boost/make_shared.hpp>

using boost::make_shared;

/* крючки-пустышки для релиза, крючки нужны для тестов */
template<typename T>
struct ProxyFlowNoCallbacks
{
    static void AfterPush(const T &val){}
    static void AfterPop(const T &val){}
};

template<typename T>//, typename CALLBACKS = ProxyFlowNoCallbacks<T> >
class ProxyFlow
{
    friend void test_proxy_flow();

public:
    typedef boost::shared_ptr<ProxyFlow> pointer;

    ProxyFlow(const std::string &name, typename Limiter<T>::pointer limiter) :
        m_name(name),
        m_limiter(limiter)
    {};

    virtual ~ProxyFlow(){};

    t_pipe_id Register()
    {
        t_scoped_lock lk(m_mutex);

        return m_limiter->Register();
    }

    void Unregister(t_pipe_id pipeId)
    {
        t_scoped_lock lk(m_mutex);

        m_limiter->Unregister(pipeId);

        m_sorter_has_data.notify_all();
    }

    void Push(t_pipe_id pipeId, const T &val)
    {
        t_scoped_lock lk(m_mutex);

        m_sorter.push(val);

        m_limiter->Push(pipeId, val);

        SDBG(m_name << " : PUSH : " << val);

        m_sorter_has_data.notify_one();
    }

    T Pop()
    {
        t_scoped_lock lk(m_mutex);

        while (IsEmpty())
        {
            SDBG(m_name << " : WAIT");
            m_sorter_has_data.wait(lk);
        }

        assert(!m_sorter.empty());

        T val = m_sorter.top();
        m_sorter.pop();

        SDBG(m_name << " : POP : " << val);

        return val;
    }

    static pointer Create(const std::string &name)
    {
        return make_shared<ProxyFlow>(name, make_shared<Limiter<T> >());
    }

    static pointer CreateUnique(const std::string &queue)
    {
        t_scoped_lock lk(st_createUniqueMutex);

        auto it = st_uniqueFlows.find(queue);

        if (it != st_uniqueFlows.end())
        {
            return it ->second;
        }

        ProxyFlow<t_data>::pointer flow = ProxyFlow<t_data>::Create(queue);

        st_uniqueFlows[queue] = flow;

        return flow;
    }

private:
    bool IsEmpty()
    {
        return m_sorter.empty() || !m_limiter->CanSend(m_sorter.top());
    }

private:
    std::string m_name;
    t_mutex m_mutex;
    t_condition m_sorter_has_data;

    std::priority_queue<T, std::vector<T>, std::greater<T> > m_sorter;

    typename Limiter<T>::pointer m_limiter;

    static t_mutex st_createUniqueMutex;
    static std::map<std::string, typename ProxyFlow::pointer> st_uniqueFlows;
};


template<typename T> t_mutex ProxyFlow<T>::st_createUniqueMutex;

template<typename T>
std::map<std::string, typename ProxyFlow<T>::pointer> ProxyFlow<T>::st_uniqueFlows;

#endif /* PROXYFLOW_H_ */
