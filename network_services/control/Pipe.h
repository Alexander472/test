#ifndef PUSHER_H_
#define PUSHER_H_

#include "config.h"
#include "readers/reader.h"
#include "writers/writer.h"

#include <boost/make_shared.hpp>

/** one way pipe, one reader - many writers */
class Pipe
{
public:
    typedef boost::shared_ptr<Pipe> pointer;

    Pipe(IReader::pointer reader, IWriter::pointer writer) :
        m_reader(reader),
        m_writers(1, writer)
    {}

    Pipe(IReader::pointer reader, const t_writers_cont &writers) :
        m_reader(reader),
        m_writers(writers)
    {}
    
    bool ProcessOne()
    {
        t_data val = 0;
        
        if (!m_reader->Read(val))
        {
            return false;
        }

        for (IWriter::pointer writer : m_writers)
        {
            writer->Write(val);
        }
        return true;
    }
    
    static pointer Create(IReader::pointer reader, IWriter::pointer writer)
    {
        return pointer(new Pipe(reader, writer));
    }
    static pointer Create(IReader::pointer reader, const t_writers_cont &writers)
    {
        return pointer(new Pipe(reader, writers));
    }

private:
    IReader::pointer m_reader;
    t_writers_cont m_writers;
};

struct PipeRunner
{
    PipeRunner(Pipe::pointer pipe);

    void operator()();

private:
    Pipe::pointer m_pipe;
};

void pause_program();
void continue_program();

#endif // PUSHER_H_
