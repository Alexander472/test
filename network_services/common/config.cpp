#include "config.h"

const char CR = '\n';

const char *FLOW_INIT_TO_ECHO = "InitToEcho";
const char *FLOW_ECHO_TO_INIT = "EchoToInit";

const char *FLOW_WRITER = "Writer";
const char *FLOW_READER = "Reader";
