#include "generator.h"

boost::shared_ptr<std::ostream> Generator::m_out;
t_data Generator::m_curr = t_data();
t_mutex Generator::m_mutex;
SIZE_T Generator::m_sleep_to = 100;

bool Generator::Read(t_data & val)
{
    t_scoped_lock lk(m_mutex);

    val = m_curr++;

    if (m_out)
    {
        *m_out << val << std::endl;
    }

    if (m_sleep_to > 0)
    {
        usleep(m_sleep_to);
    }

    return true;
}

IReader::pointer Generator::Create()
{
    return boost::make_shared<Generator>();
}

void Generator::SetStream(boost::shared_ptr<std::ostream> s)
{
    m_out = s;
}

void Generator::SetSleepTimeout(size_t usecs)
{
    m_sleep_to = usecs;
}
