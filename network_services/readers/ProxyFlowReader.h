#ifndef PROXYFLOWREADER_H_
#define PROXYFLOWREADER_H_

#include "readers/reader.h"
#include "control/ProxyFlow.h"

class ProxyFlowReader: public IReader
{
public:
    ProxyFlowReader(ProxyFlow<t_data>::pointer proxyFlow) :
        IReader(),
        m_proxyFlow(proxyFlow)
    {}

    virtual bool Read(t_data &val)
    {
        val = m_proxyFlow->Pop();

        return true;
    }

    static IReader::pointer Create(ProxyFlow<t_data>::pointer proxyFlow)
    {
        return make_shared<ProxyFlowReader>(proxyFlow);
    }

private:
    ProxyFlow<t_data>::pointer m_proxyFlow;
};

#endif /* PROXYFLOWREADER_H_ */
