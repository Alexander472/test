#ifndef READER_H_
#define READER_H_

#include "config.h"

class IReader
{
public:
    typedef boost::shared_ptr<IReader> pointer;

    virtual ~IReader() {};

    virtual bool Read(t_data &val) = 0;
};

#endif // READER_H_
