#ifndef SOCKETREADER_H_
#define SOCKETREADER_H_

#include <boost/asio.hpp>
#include "readers/reader.h"
#include "common/mylog.h"

class SocketReader: public IReader
{
public:
    SocketReader(socket_ptr sock) :
        IReader(),
        m_sock(sock)
    {}

    virtual bool Read(t_data &val)
    {
        boost::asio::read_until(*m_sock, m_data, CR);

        std::istream m_is(&m_data);

        m_is >> val;

        std::string s;
        getline(m_is, s);

        SDBG("R:" << val);

        return true;
    }

    static boost::shared_ptr<SocketReader> Create(socket_ptr sock)
    {
        return boost::shared_ptr<SocketReader>(new SocketReader(sock));
    }

private:
    socket_ptr m_sock;
    boost::asio::streambuf m_data;
};

#endif /* SOCKETREADER_H_ */
