
#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include "generator.h"

using namespace std;

TEST_CASE("pipe/StepNext", "")
{
    IReader::pointer gen = Generator::Create();
    
    t_data val = 0;
    
    REQUIRE(gen->Read(val));
    REQUIRE(0 == val);
    REQUIRE(gen->Read(val));
    REQUIRE(1 == val);
    REQUIRE(gen->Read(val));
    REQUIRE(2 == val);
    REQUIRE(gen->Read(val));
    REQUIRE(3 == val);
}
