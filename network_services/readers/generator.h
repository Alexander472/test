#ifndef GENERATOR_H_
#define GENERATOR_H_

#include "config.h"

#include "readers/reader.h"
#include "auto_value.h"

class Generator : public IReader
{
public:
    Generator() : IReader()
    {}

    virtual bool Read(t_data &val);

    static IReader::pointer Create();

    static void SetStream(boost::shared_ptr<std::ostream> s);
    static void SetSleepTimeout(size_t usecs);
private:
    static boost::shared_ptr<std::ostream> m_out;
    static t_data m_curr;
    static t_mutex m_mutex;
    static SIZE_T m_sleep_to;
};

#endif // GENERATOR_H_
