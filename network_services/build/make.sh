#!/bin/bash

cmake .. -Wno-dev || exit 1
make || exit 2
ctest || exit 3
echo "!!!!!!!!!!!!Build Successful!!!!!!!!!!!"