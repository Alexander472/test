#include "control/Pipe.h"

#include "control/ProxyFlow.h"
#include "control/Controller.h"
#include "readers/generator.h"
#include "readers/SocketReader.h"
#include "readers/ProxyFlowReader.h"
#include "writers/ProxyFlowWriter.h"
#include "writers/StreamWriter.h"
#include "writers/SocketWriter.h"
#include "transport/Handshake.h"

#include <fstream>
#include <boost/thread.hpp>
#include <boost/lexical_cast.hpp>

using namespace std;
using namespace boost::asio;

io_service my_io_service;

int main(int argc, char *argv[])
{
    if (!(argc == 5 || argc == 6))
    {
        cerr << "Usage: initiator_server <proxy address> <proxy port> <sending threads count> <recieving threads count> [sleep timeout]" << endl;
        cerr << endl;
        cerr << "Ex.: initiator_server 127.0.0.1 5000 2 3" << endl;
        cerr << " - connect to proxy 127.0.0.1 to port 5000 and use 2 sending and 3 recieving threads" << endl;
        cerr << " - [sleep timeout] is a timeout in microseconds before next number will be generated, default = 100 microseconds" << endl;
        return EXIT_FAILURE;
    }

    const size_t writers_count = boost::lexical_cast<size_t>(argv[3]);
    const size_t readers_count = boost::lexical_cast<size_t>(argv[4]);

    if (6 == argc)
    {
        Generator::SetSleepTimeout(boost::lexical_cast<size_t>(argv[5]));
    }

    boost::shared_ptr<std::ofstream> gen_out(new std::ofstream("initiator_send.txt"));
    Generator::SetStream(gen_out);

    Controller controller("Initiator Server", boost::make_shared<std::istream>(cin.rdbuf()));

    ProxyFlow<t_data>::pointer output_file_flow = ProxyFlow<t_data>::Create("initiator_recieve.txt");

    ip::tcp::resolver resolver(my_io_service);
    ip::tcp::resolver::query query(argv[1], argv[2]);

    for (size_t i = writers_count; i > 0; --i)
    {
        socket_ptr socket(new ip::tcp::socket(my_io_service));
        boost::asio::connect(*socket, resolver.resolve(query));
        Handshake(socket, FLOW_INIT_TO_ECHO, FLOW_WRITER);

        controller.AddPipe(Pipe::Create(Generator::Create(), SocketWriter::Create(socket)));
    }

    for (size_t i = readers_count; i > 0; --i)
    {
        socket_ptr socket(new ip::tcp::socket(my_io_service));
        boost::asio::connect(*socket, resolver.resolve(query));
        Handshake(socket, FLOW_ECHO_TO_INIT, FLOW_READER);

        controller.AddPipe(Pipe::Create(SocketReader::Create(socket), ProxyFlowWriter::Create(output_file_flow)));
    }

    controller.AddPipe(Pipe::Create(ProxyFlowReader::Create(output_file_flow), StreamWriter::Create(boost::make_shared<std::ofstream>("initiator_recieve.txt"))));

    controller.Run();

    return EXIT_SUCCESS;
}
