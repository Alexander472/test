function(unit_test test)
    add_test(${test} ${EXECUTABLE_OUTPUT_PATH}/${test})
    add_executable(${test} ${test}.cpp ${ARGN})
endfunction()

function(add_binary name)
    add_executable(${name} ${ARGN})
    target_link_libraries(${name} readers writers control common transport)
endfunction()
