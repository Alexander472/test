#include "connection.hpp"
#include <vector>
#include <boost/bind.hpp>
#include <boost/thread/thread.hpp>

connection::connection(boost::asio::io_service& io_service)
  : strand_(io_service),
    socket_(io_service)
{
}

boost::asio::ip::tcp::socket& connection::socket()
{
  return socket_;
}

void connection::start()
{
/*
 *   socket_.async_read_some(boost::asio::buffer(buffer_),
      strand_.wrap(
        boost::bind(&connection::handle_read, shared_from_this(),
          boost::asio::placeholders::error,
          boost::asio::placeholders::bytes_transferred)));
*/

  socket_.read_some(boost::asio::buffer(buffer_),
        boost::bind(&connection::handle_read, shared_from_this(),
          boost::asio::placeholders::error,
          boost::asio::placeholders::bytes_transferred));
}

void connection::handle_read(const boost::system::error_code& e,
    std::size_t bytes_transferred)
{
    if (!e)
    {
        std::string s(&buffer_[0], &buffer_[bytes_transferred]);
        std::cout << boost::this_thread::get_id() << ":" << bytes_transferred << ":" << s;

        start();
    }
}

void connection::handle_write(const boost::system::error_code& e)
{
  if (!e)
  {
    // Initiate graceful connection closure.
    boost::system::error_code ignored_ec;
    socket_.shutdown(boost::asio::ip::tcp::socket::shutdown_both, ignored_ec);
  }
}
