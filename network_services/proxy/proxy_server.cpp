#include "readers/generator.h"
#include "writers/writer.h"
#include "control/Pipe.h"

#include "config.h"
#include "control/ProxyFlow.h"
#include "control/Controller.h"
#include "readers/ProxyFlowReader.h"
#include "readers/SocketReader.h"
#include "writers/ProxyFlowWriter.h"
#include "writers/SocketWriter.h"
#include "writers/ConsoleWriter.h"

#include <fstream>
#include <boost/thread.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/bind.hpp>

using namespace boost;
using namespace boost::asio;

static io_service my_io_service;

Pipe::pointer CreatePusher(socket_ptr sock, const std::string &queue, const std::string &type)
{
    ProxyFlow<t_data>::pointer flow = ProxyFlow<t_data>::CreateUnique(queue);

    if (FLOW_READER == type)
    {
        return Pipe::Create(ProxyFlowReader::Create(flow), SocketWriter::Create(sock));
    }

    if (FLOW_WRITER == type)
    {
        return Pipe::Create(SocketReader::Create(sock), ProxyFlowWriter::Create(flow));
    }

    assert(0);
//    THROW_BUG("unknown client type : " << PR(queue) << PR(type));
}


Pipe::pointer handshake(socket_ptr m_sock)
{
    boost::system::error_code error;

    std::string queue;
    std::string type;

    {
        boost::asio::streambuf b;

        boost::asio::read_until(*m_sock, b, '\n');
        std::istream is(&b);

        is >> queue >> type;
    }

    Pipe::pointer pipe = CreatePusher(m_sock, queue, type);

    std::cerr << "Q:" << queue << " T:" << type << std::endl;

    {
        boost::asio::streambuf b;

        std::ostream os(&b);

        os << "ACK" << std::endl;

        boost::asio::write(*m_sock, b);
    }

    return pipe;
};

void accept_connections(Controller &controller, const char *addr, const char *port)
{
    ip::tcp::resolver resolver(my_io_service);
    ip::tcp::resolver::query query(addr, port);
    ip::tcp::endpoint endpoint = *resolver.resolve(query);

    ip::tcp::acceptor acceptor(my_io_service);

    acceptor.open(endpoint.protocol());
    acceptor.set_option(ip::tcp::acceptor::reuse_address(true));
    acceptor.bind(endpoint);
    acceptor.listen();

    for(;;)
    {
        socket_ptr socket(new ip::tcp::socket(my_io_service));

        acceptor.accept(*socket);

        controller.AddPipe(handshake(socket));
    }
}

int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        std::cerr << "Usage: proxy_server <address> <port>" << std::endl;
        return EXIT_FAILURE;
    }

    boost::thread_group m_threads;

    ProxyFlow<t_data>::pointer system_log_flow = ProxyFlow<t_data>::Create("System");

    Controller controller("ProxyServer", boost::make_shared<std::istream>(std::cin.rdbuf()));

    controller.AddPipe(Pipe::Create(ProxyFlowReader::Create(system_log_flow),
            ConsoleWriter::Create()));

    boost::thread controller_thread(bind(&Controller::Run, ref(controller)));
    boost::thread accept_connections_thread(accept_connections, ref(controller), argv[1], argv[2]);

    /* wait for user exit-command */
    controller_thread.join();

//    accept_connections_thread.interrupt();
//    accept_connections_thread.join();

    return EXIT_SUCCESS;
}
