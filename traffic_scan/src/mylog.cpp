#include "mylog.h"

static LOG_LEVEL st_curr_log_level = LOG_ERROR;
static boost::shared_ptr<std::ostream> st_log_stream;
t_mutex log_mutex;

bool is_log_level_allowed(LOG_LEVEL level)
{
    /* dont lock */
    return level <= st_curr_log_level;
}

boost::shared_ptr<std::ostream> log_stream()
{
    return st_log_stream;
}

void set_log_stream(boost::shared_ptr<std::ostream> s)
{
    t_scoped_lock lk(log_mutex);
    st_log_stream = s;
}

void set_log_level(LOG_LEVEL level)
{
    t_scoped_lock lk(log_mutex);

    st_curr_log_level = level;
}
