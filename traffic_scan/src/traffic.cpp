#include "traffic.h"
#include "config.h"

#include <map>

struct Traffic
{
    void Push(size_t port)
    {
        t_scoped_lock lk(m_mutex);

        auto it = m_packetsCount.find(port);
        if (it != m_packetsCount.end())
        {
            ++it->second;
        }
        else
        {
            m_packetsCount[port] = 1;
        }
    }

    std::map<size_t, size_t> Get()
    {
        t_scoped_lock lk(m_mutex);

        const std::map<size_t, size_t> res = m_packetsCount;

        m_packetsCount.clear();

        return res;
    }

private:
    t_mutex m_mutex;
    std::map<size_t, size_t> m_packetsCount;
};

static Traffic g_traffic;

void PushTcpPacketPort(size_t port)
{
    g_traffic.Push(port);
}

std::map<size_t, size_t> GetTcpPortsPacketsTotal()
{
    return g_traffic.Get();
}
