#ifndef CONFIG_H_
#define CONFIG_H_

#include <boost/thread/mutex.hpp>
#include <boost/thread/locks.hpp>
#include <boost/thread/condition.hpp>


typedef boost::mutex::scoped_lock t_scoped_lock;
typedef boost::mutex t_mutex;
typedef boost::condition t_condition;

#endif // CONFIG_H_
