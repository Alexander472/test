#ifndef BASE_ERROR_H_
#define BASE_ERROR_H_

#include <exception>
#include <string>
#include <sstream>
#include <typeinfo>
#include <string.h>

/** Базовый класс наших исключений */
struct my_error : public std::exception
{
    template <typename E>
    friend
    E where(E ex, const char *name, const char *file, const char *func, int line, const std::string what = std::string(""))
    {
        ex.where(name, file, func, line, what);
        return ex;
    }

    void where(const char *name, const char *file, const char *func, int line, const std::string &what)
    {
        std::ostringstream os;
        os << name << "::" << file << "::" << func << "(" << line << ")";
        if (!what.empty())
        {
            os << " : " << what;
        }
        m_errMsg = os.str();
    }
//    my_error(const std::string &errMsg) : std::exception(), m_errMsg(errMsg) {};
    virtual ~my_error() throw() {}

    virtual const char * what() const throw() { return m_errMsg.c_str(); }
private:
    std::string m_errMsg;
};

struct bug_error : public my_error {}; ///< наша ошибка, требуется доработка программы; опасно выполнять программу дальше.
struct runtime_error : public my_error {}; ///< базовый класс ошибок выполнения (ожидаемых)
struct io_error : public runtime_error {}; ///< ошибки I/O
struct config_error : public runtime_error {}; ///< ошибки конфигурирования
struct user_error : public runtime_error {}; ///< ошибки ввода пользователем некоректных данных

std::string sys_error(); ///< получить последнюю ошибку

#define MYTHROW(ex) { throw where(ex, typeid(ex).name(), __FILE__, __FUNCTION__, __LINE__); }
#define MYTHROW_MSG(ex, what) { std::ostringstream os; os << what; throw where((ex), typeid(ex).name(), __FILE__, __FUNCTION__, __LINE__, os.str()); }

#define PR(a) #a << " = " << a

/** Проверки утверждений, замена assert на исключения */
#define SOFT_CHECK(a) if (!(a)) { MYTHROW_MSG(bug_error(), PR((a))); }
#define SOFT_CHECK_EQ(a, b) if (!((a) == (b))) { MYTHROW_MSG(bug_error(), PR((a)) << " == " << PR((b))); }
#define SOFT_CHECK_NE(a, b) if (!((a) != (b))) { MYTHROW_MSG(bug_error(), PR((a)) << " != " << PR((b))); }

#define CHECK_OPERS_THROW_IMPL(a, b, op, ex, msg) { MYTHROW_MSG((ex), "(" <<  PR((a)) << ")" << (op) << "(" << PR((b)) << ")" << " : " << (msg)); }
/** Проверки корректности результатов работы  */
#define CHECK_NE(a, b, ex, msg) if (!((a) != (b))) { CHECK_OPERS_THROW_IMPL((a), (b), " != ", (ex), (msg)); }
#define CHECK_GT(a, b, ex, msg) if (!((a) > (b))) { CHECK_OPERS_THROW_IMPL((a), (b), " > ", (ex), (msg)); }
#define CHECK_EQ(a, b, ex, msg) if (!((a) == (b))) { CHECK_OPERS_THROW_IMPL((a), (b), " == ", (ex), (msg)); }

#endif /* BASE_ERROR_H_ */
