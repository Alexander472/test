#ifndef TRAFFIC_H_
#define TRAFFIC_H_

#include <map>
#include <stddef.h>

/**
 * Добавить tcp порт в статистику принятых пакетов
 * @param port TCP-port
 */
void PushTcpPacketPort(size_t port);

/**
 * Получить статистику кол-ва пакетов по TCP-портам
 * @return
 */
std::map<size_t, size_t> GetTcpPortsPacketsTotal();

#endif /* TRAFFIC_H_ */
