#include "network.h"
#include "base_error.h"
#include "mylog.h"
#include "traffic.h"

#include <string>
#include <boost/shared_ptr.hpp>

using std::string;

std::string get_device_ip(const std::string &devName)
{
    pcap_if_t *alldevs = NULL;
    char errbuf[PCAP_ERRBUF_SIZE];

    /* The user didn't provide a packet source: Retrieve the device list */
    if (pcap_findalldevs(&alldevs, errbuf) == -1)
    {
        MYTHROW_MSG(io_error(), "Error in pcap_findalldevs: "  << errbuf);
    }

    boost::shared_ptr<pcap_if_t> free_it_on_exit(alldevs, pcap_freealldevs);

    /* Print the list */
    for(pcap_if_t *d = alldevs; d != NULL; d = d->next)
    {
        SDBG(PR(d->name));
        if (string(d->name) == devName)
        {
            for (pcap_addr_t *a = d->addresses; a != NULL; a = a->next)
            {
                if(a->addr->sa_family == AF_INET)
                {
                    return inet_ntoa(((struct sockaddr_in*)a->addr)->sin_addr);
                }
            }
            MYTHROW_MSG(io_error(), "no IPv4 for device " << devName);
        }
    }
    MYTHROW_MSG(user_error(), "device not found : "  << devName);
}

void got_packet(u_char *args, const struct pcap_pkthdr *header, const u_char *packet)
{
    /* define/compute ip header offset */
    const struct sniff_ip *ip = (struct sniff_ip*)(packet + SIZE_ETHERNET);
    const size_t size_ip = IP_HL(ip)*4;
    if (size_ip < 20) {
        SERR("   * Invalid IP header length: " << size_ip << " bytes");
        return;
    }

    /* determine protocol */
    if (ip->ip_p != IPPROTO_TCP)
    {
        return;
    }

    /* define/compute tcp header offset */
    const struct sniff_tcp *tcp = (struct sniff_tcp*)(packet + SIZE_ETHERNET + size_ip);
    const size_t size_tcp = TH_OFF(tcp)*4;
    if (size_tcp < 20) {
        SERR("   * Invalid TCP header length: " << size_tcp << " bytes");
        return;
    }

    SINFO(string(inet_ntoa(ip->ip_src)) << ":" << ntohs(tcp->th_sport)
            << " -> " << string(inet_ntoa(ip->ip_dst)) << ":" << ntohs(tcp->th_dport));

    PushTcpPacketPort(ntohs(tcp->th_dport));
}
