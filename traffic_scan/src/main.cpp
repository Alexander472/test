#include "network.h"
#include "base_error.h"
#include "mylog.h"
#include "traffic.h"

#include <pcap.h>
#include <signal.h>

#include <iostream>
#include <string>
#include <sstream>
#include <boost/shared_ptr.hpp>
#include <boost/date_time.hpp>

using namespace std;

boost::shared_ptr<pcap_t> handle; /* Session handle */

void terminate_hanlder(int signo)
{
    if (handle)
    {
        pcap_breakloop(handle.get());
    }
}

/** функция освобождения с проверкой для shared_ptr */
void pcap_close_if_not_null(pcap_t *p)
{
    if (p)
    {
        pcap_close(p);
    }
}

void print_thread_handler()
{
    while (!boost::this_thread::interruption_requested())
    {
        std::map<size_t, size_t> packets = GetTcpPortsPacketsTotal();

        cout << boost::posix_time::to_simple_string(boost::posix_time::second_clock::local_time()) << endl;
        for (auto i : packets)
        {
            cout << i.first << "  --->   " << i.second << endl;
        }

        cout << endl;

        boost::this_thread::sleep(boost::posix_time::seconds(1));
    }
}


int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        cerr << "Usage : trafic_scan <ethernet device>" << endl;
        return EXIT_FAILURE;
    }

    try
    {
        signal(SIGINT, &terminate_hanlder);
        signal(SIGTERM, &terminate_hanlder);
        signal(SIGQUIT, &terminate_hanlder);

        char *dev = argv[1]; /* The device to sniff on */
        char errbuf[PCAP_ERRBUF_SIZE]; /* Error string */
        bpf_u_int32 mask = 0; /* Our netmask */
        bpf_u_int32 net = 0;

        /* Define the device */
        dev = argv[1];

        /* Find the properties for the device */
        if (pcap_lookupnet(dev, &net, &mask, errbuf) == -1)
        {
            SERR("Couldn't get netmask for device  : " << dev << " : " << errbuf);
            net = 0;
            mask = 0;
        }
        /* Open the session in promiscuous mode */
        handle.reset(pcap_open_live(dev, BUFSIZ, 1, 1000, errbuf), &pcap_close_if_not_null);

        if (!handle)
        {
            MYTHROW_MSG(io_error(), "Couldn't open device : " << dev << " : " << errbuf);
        }

        ostringstream filter;
        filter << "tcp and dst host " << get_device_ip(dev);

        SDBG(PR(filter.str()));

        /* Compile and apply the filter */
        struct bpf_program fp; /* The compiled filter */
        if (pcap_compile(handle.get(), &fp, filter.str().c_str(), 0, net) == -1)
        {
            MYTHROW_MSG(io_error(), "Couldn't parse filter : " << filter << " : " << pcap_geterr(handle.get()));
        }
        if (pcap_setfilter(handle.get(), &fp) == -1)
        {
            MYTHROW_MSG(io_error(), "Couldn't install filter : " << filter << " : " << pcap_geterr(handle.get()));
        }

        boost::thread print_thread(&print_thread_handler);

        pcap_loop(handle.get(), 0, got_packet, NULL);

        print_thread.interrupt();
        print_thread.join();

        cerr << "done" << endl;
    }
    catch(const std::exception &e)
    {
        SERR("Error : " << e.what());

        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

