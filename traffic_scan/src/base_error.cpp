#include "base_error.h"

#include <errno.h>
#include <sstream>

std::string sys_error()
{
    const int err = errno;
    std::ostringstream os;
    os << err << ", "<< strerror(err);
    return os.str();
}

