#ifndef MYLOG_H_
#define MYLOG_H_

/** Simple log, in production system will use log4cpp */
#include "config.h"
#include <ostream>
#include <boost/thread.hpp>

enum LOG_LEVEL
{
    LOG_ERROR,
    LOG_INFO,
    LOG_DEBUG
};

boost::shared_ptr<std::ostream> log_stream();
void set_log_stream(boost::shared_ptr<std::ostream> s);
void set_log_filename(const std::string &fname);
void set_log_level(LOG_LEVEL level);
bool is_log_level_allowed(LOG_LEVEL level);

extern t_mutex log_mutex;

#define SLOG(level, output_strings) \
    if (is_log_level_allowed(level)) \
    { \
        t_scoped_lock lk(log_mutex); \
        boost::shared_ptr<std::ostream> ps = log_stream(); \
        (ps ? *ps : std::cerr ) << boost::this_thread::get_id() << ":" << __FUNCTION__ << "(" << __LINE__ << ") : " \
                << output_strings << std::endl; \
    }

#define SERR(output_strings) SLOG(LOG_ERROR, output_strings)
#define SINFO(output_strings) SLOG(LOG_INFO, output_strings)
#define SDBG(output_strings) SLOG(LOG_DEBUG, output_strings)

#endif /* MYLOG_H_ */
