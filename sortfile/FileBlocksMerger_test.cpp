#include "FileBlocksMerger.h"
#include "FileStub.h"

#define CATCH_CONFIG_MAIN
#include "catch.hpp"

using namespace std;

TEST_CASE("ReadIterator/Iterate", "")
{
    t_File file = make_shared<FileStub>("One");

    ReadIterator<char> it(file);

    REQUIRE(it);
    REQUIRE('O' == it++.GetValue());
    REQUIRE(it);
    REQUIRE('n' == it++.GetValue());
    REQUIRE(it);
    REQUIRE('e' == it++.GetValue());
    REQUIRE_FALSE(it);
}

TEST_CASE("ReadIterator/Compare", "")
{
    ReadIterator<char> itA(make_shared<FileStub>("1"));
    ReadIterator<char> itB(make_shared<FileStub>("2"));

    REQUIRE(itA);
    REQUIRE(itB);

    REQUIRE(itA < itB);
    REQUIRE_FALSE(itB < itA);
    REQUIRE(itB > itA);
    REQUIRE_FALSE(itA > itB);
}

TEST_CASE("FileBlocksMerger", "")
{
    vector<t_File> blockFiles;
    blockFiles.push_back(make_shared<FileStub>("159"));
    blockFiles.push_back(make_shared<FileStub>("368"));
    blockFiles.push_back(make_shared<FileStub>("0247"));

    t_FileStub dst = make_shared<FileStub>("");

    FileBlocksMerger<char> merger(blockFiles, dst);

    merger.MergeBlocksToFile();

    REQUIRE("0123456789" == dst->Str());
}
