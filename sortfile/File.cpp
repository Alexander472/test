#include "File.h"
#include <assert.h>
#include <stdio.h>

size_t File::Read(char* buf, size_t size)
{
    const size_t res = fread(buf, 1, size, m_fd);

    // CHECK(ferror(m_fd));

    return res;
}

size_t File::Write(const char* buf, size_t size)
{
    const size_t res = fwrite(buf, size, 1, m_fd);

    CHECK_EQ(1, res, io_error(), sys_error());

    return res;
}

void File::Reset()
{
    int res = fseek(m_fd, 0, SEEK_SET);

    CHECK_NE(-1, res, io_error(), sys_error());
}

