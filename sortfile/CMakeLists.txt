enable_testing()

function(unit_test test)
    add_test(${test} ${EXECUTABLE_OUTPUT_PATH}/${test})
    add_executable(${test} ${test}.cpp ${ARGN})
endfunction()

add_definitions(-std=c++0x -g -O0 -Wall)

unit_test(FileBlocksMerger_test)
unit_test(FileSplitter_test)

add_executable(sortfile main.cpp File.cpp base_error.cpp)
