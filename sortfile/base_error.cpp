#include "base_error.h"

std::string sys_error()
{
    const int err = errno;
    std::ostringstream os;
    os << err << ", "<< strerror(err);
    return os.str();
}

