#include "File.h"

using namespace std;


/** Класс для тестирования.
  *  Позволяет подставить данные файла и получить щзаписанные данные в файл
  */
class FileStub : public IFile
{
public:
    /** Создать файл с заданным содержимым, текущую позицию поставить на начало
     * @param s содержимое файла
    */
    FileStub(const std::string &s) :
        m_str(s),
        m_iter(m_str.begin())
    {}

    virtual size_t Read(char *buf, size_t size)
    {
        size_t i = 0;
        for (; i < size; ++i)
        {
            if (m_str.end() == m_iter)
            {
                break;
            }
            buf[i] = *m_iter++;
        }
        return i;
    }

    virtual size_t Write(const char *buf, size_t size)
    {
        m_str.append(buf, size);
        return size;
    }

    virtual void Reset()
    {
        m_iter = m_str.begin();
    }

    /** Получить содержимое файла 
     * @return содержимое файла
    */
    string Str() const
    {
        return m_str;
    }

    /** Очистить содержимое файла, текущую позицию переместить на начало */
    void Clear()
    {
        m_str.clear();
        m_iter = m_str.begin();
    }
private:
    string m_str; // содержимое файла
    string::iterator m_iter; // текущая позиция в файле
};

typedef shared_ptr<FileStub> t_FileStub;
