#ifndef FILE_H_
#define FILE_H_

#include "base_error.h"

#include <memory>
#include <string>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>
#include <iostream>

/* часть <boost/make_shared.hpp> */
template<typename T> std::shared_ptr<T> make_shared()
{
    return std::shared_ptr<T>(new T());
}
template<typename T, typename A1, typename A2> std::shared_ptr<T> make_shared(const A1 &a1, const A2 &a2)
{
    return std::shared_ptr<T>(new T(a1, a2));
}

/** Параметры создания файла */
struct FileParams
{
    FileParams(const std::string &fname) :
        m_fname(fname),
        m_readOnly(false),
        m_create(false)
    {};

    FileParams & ReadOnly() { m_readOnly = true; return *this; }
    bool IsReadOnly() const { return m_readOnly; }

    FileParams & FileName(const std::string &fname) { m_fname = fname; return *this; }
    std::string FileName() const { return m_fname; }

    FileParams & Create() { m_create = true; return *this; }
    bool & IsCreate() { return m_create; }

private:

    std::string m_fname;
    bool m_readOnly;
    bool m_create;
};

/* стратегии обработки файлов */
class IFileOpersStrategy
{
public:
    /**
     * Открыть файл с заданными параметрами
     * @param params параметры открытия файла
     * @return дескриптор
     * @exception io_error
     */
    FILE *Open(FileParams &params) const
    {
        FILE *fd = doOpen(params);

        CHECK_NE(0, fd, io_error(), PR(params.FileName()) << ", " << sys_error());

        return fd;
    }

    /**
     * Закрыть файл
     * @param fd файловый дескриптор открытого ранее файла
     * @param params параметры закрытия файла
     */
    void Close(FILE *fd, const FileParams &params)
    {
        if (fd != NULL)
        {
            fclose(fd);
        }
    }

private:
    virtual FILE *doOpen(FileParams &params) const = 0;
};

typedef std::shared_ptr<IFileOpersStrategy> t_file_opers_strategy;

/** Стратегия работы с обычными файлами */
class FileOpersStrategySimple : public IFileOpersStrategy
{
    virtual FILE *doOpen(FileParams &params) const
    {
        const char *mode = params.IsReadOnly() ? "r"
                : (params.IsCreate() ? "w+" : "a");

        return fopen(params.FileName().c_str(), mode);
    }
};

/** Стратегия работы со временными файлами */
class FileOpersStrategyTmpFile : public IFileOpersStrategy
{
    virtual FILE *doOpen(FileParams &params) const
    {
        FILE *fd = tmpfile();

        CHECK_NE(0, fd, io_error(), sys_error());

        return fd;
    }
};

/** Интерфейс работы с файлами  */
class IFile
{
public:
    /** Прочитать в буфер из файла заданное кол-во байт
      * @param buf буфер
      * @param size кол-во читаемых байт
      * @return сколько фактически прочитали
      */
    virtual size_t Read(char *buf, size_t size) = 0;
    /** Записать заднное кол-во байт из буфера в файл
      * @param buf буфер
      * @param size кол-во записываемых байт
      * return сколько байтов записали
      */
    virtual size_t Write(const char *buf, size_t size) = 0;

    /** Прочитать значение заданного типа из файла
      * @param val куда и значение какого типа читаем
      * @return true - успешно прочитали, false - конец файла
      */
    template<typename T> bool Read(T &val)
    {
        return sizeof(T) == Read((char *)&val, sizeof(T));
    }

    /** Прочитать подряд заданное кол-во значений указанного типа из файла
      * @param arr куда и значения какого типа читаем
      * @param arrSize кол-во читаемых значений
      * @return кол-во прочитанных значений
      */
    template<typename T> size_t Read(T arr[], size_t arrSize)
    {
        return Read((char *)arr, arrSize * sizeof(T)) / sizeof(T);
    }

    /** Записать значение указанного типа в файл
      * @param val записываемое значение
      */
    template<typename T> void Write(const T &val)
    {
        Write((char *)&val, sizeof(T));
    }

    /** Записать подряд заданное кол-во значений указанного типа в файла
      * @param arr откуда и значения какого типа пишем
      * @param arrSize кол-во записываемых значений
      */
    template<typename T> void Write(const T arr[], size_t arrSize)
    {
        Write((char *)arr, arrSize * sizeof(T));
    }

    virtual ~IFile()
    {
    }

    virtual void Reset() = 0;
};

typedef std::shared_ptr<IFile> t_File;

/** Физический файл */
class File : public IFile
{
public:
    virtual size_t Read(char *buf, size_t size);

    virtual size_t Write(const char *buf, size_t size);


    /** Открыть файл с указанными параметрами
      * @param params параметры открытия файла
      */
    static t_File Create(const FileParams &params)
    {
        return std::shared_ptr<File>(new File(params, make_shared<FileOpersStrategySimple>()));
    }

    /** Открыть временный файл с указанными параметрами
      * @param params параметры открытия файла
      */
    static t_File CreateTmpFile(const FileParams &params)
    {
        return std::shared_ptr<File>(new File(params, make_shared<FileOpersStrategyTmpFile>()));
    }

    virtual ~File()
    {
        m_fileOpers->Close(m_fd, m_params);
    }

    virtual void Reset();

protected:
    File(const FileParams &params, t_file_opers_strategy fileOpers) :
        IFile(),
        m_params(params),
        m_fileOpers(fileOpers),
        m_fd(NULL)
    {
        m_fd = m_fileOpers->Open(m_params);
        SOFT_CHECK_NE(0, m_fd);
    }

private:
    File();
    File & operator=(const File &);

private:
    FileParams m_params;
    t_file_opers_strategy m_fileOpers;
    FILE *m_fd;
};

#endif /* FILE_H_ */
