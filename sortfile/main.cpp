#include "File.h"
#include "FileSplitter.h"
#include "FileBlocksMerger.h"

#include <assert.h>
#include <vector>
#include <iostream>

using namespace std;

typedef uint32_t t_data; // тип сортируемых данных

template<typename T>
void print_file(const std::string &fname)
{
    t_File srcFile = File::Create(FileParams(fname).ReadOnly());

    ReadIterator<T> it(srcFile);

    cout << "Print " << fname << endl;
    cout << "---------------------------------------------" << endl;

    while(it)
    {
        cout << it++.GetValue() << endl;
    }
    cout << "---------------------------------------------" << endl;
}

void usage()
{
    cerr << "Usage: sortfile <source file> <destination file> <sort block size>" << endl;
}

struct Config
{
    Config& SourceFileName(const std::string &fname)
    {
        m_srcName = fname;
        return *this;
    }

    Config& DestinationFileName(const std::string &fname)
    {
        m_dstName = fname;
        return *this;
    }

    Config& BlockSize(const std::string &blockSize)
    {
        int size = atoi(blockSize.c_str());

        CHECK_GT(size, 0, config_error(), "blockSize have to be greater then zero");
        m_blockSize = size;
        return *this;
    }

    std::string SourceFileName() const { return m_srcName; }
    std::string DestinationFileName() const { return m_dstName; }
    size_t BlockSize() const { return m_blockSize; }
    std::string TmpFileTemplate() const { return "/tmp/sortfileXXXXXX"; }

private:
    std::string m_srcName;
    std::string m_dstName;
    size_t m_blockSize;
};

const char *my_ctime()
{
    time_t t = time(NULL);
    return ctime(&t);
}
template<typename T>
void sortfile(const Config &config)
{
    cout << "start : " << my_ctime() << endl;
    vector<t_File> blockFiles;

    t_File srcFile = File::Create(FileParams(config.SourceFileName()).ReadOnly());

    while (true)
    {
        t_File blockFile = File::CreateTmpFile(FileParams(config.TmpFileTemplate()));
        blockFiles.push_back(blockFile);


        FileSplitter<T> splitter(srcFile, blockFile, config.BlockSize());

        if (!splitter.SplitOneBlock())
        {
            break;
        }
    }

    for (auto &file : blockFiles)
    {
        file->Reset();
    }

    cout << "done split : " << my_ctime() << endl;

    t_File dstFile = File::Create(FileParams(config.DestinationFileName()).Create());

    FileBlocksMerger<T> merger(blockFiles, dstFile);

    merger.MergeBlocksToFile();

    cout << "done merge : " << my_ctime() << endl;
}

template<typename T>
int my_main(int argc, char *argv[])
{
    if (argc != 4)
    {
        usage();
        return EXIT_FAILURE;
    }

    char **p = &argv[1];

    Config config;

    config.SourceFileName(*p++);
    config.DestinationFileName(*p++);
    config.BlockSize(*p++);

    sortfile<T>(config);

//    print_file<T>(config.SourceFileName());
//    print_file<T>(config.DestinationFileName());

    return EXIT_SUCCESS;
}

int main(int argc, char *argv[])
{
    int result = EXIT_FAILURE;
    try
    {
        result = my_main<t_data>(argc, argv);
    }
    catch(const runtime_error &e)
    {
        cerr << "Error : " << e.what() << endl;
    }
    return result;
}
