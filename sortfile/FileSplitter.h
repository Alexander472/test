#ifndef FILESPLITTER_H_
#define FILESPLITTER_H_

#include "File.h"

#include <memory>
#include <algorithm>

/**
 * Разбивает файл на меньшие отсортированные файлы указанного размера
 */
template<typename T>
class FileSplitter
{
    typedef T data_type;
public:
    FileSplitter(t_File srcFile, t_File dstFile, size_t sizeMax) :
        m_srcFile(srcFile),
        m_dstFile(dstFile),
        m_sizeMax(sizeMax)
    {
    }

    bool SplitOneBlock()
    {
        std::unique_ptr<data_type []> buf(new data_type [m_sizeMax]);

        size_t readSize = m_srcFile->Read(buf.get(), m_sizeMax);

        if (readSize == 0)
        {
            // EOF
            return false;
        }

        std::sort(&buf[0], &buf[readSize]);

        m_dstFile->Write(buf.get(), readSize);

        return m_sizeMax == readSize;
    }
    virtual ~FileSplitter(){};

private:
    t_File m_srcFile;
    t_File m_dstFile;
    size_t m_sizeMax;
};

#endif /* FILESPLITTER_H_ */
