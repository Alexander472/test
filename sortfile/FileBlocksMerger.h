#ifndef FILEBLOCKSMERGER_H_
#define FILEBLOCKSMERGER_H_

#include "File.h"

#include <vector>
#include <queue>
#include <iostream>
#include "base_error.h"

/** Итератор чтения из файла */
template<typename T>
struct ReadIterator
{
    typedef T data_type;

    /**
     * Создать итератор по файлу
     * @param file итерируемый файл
     */
    ReadIterator(t_File file) :
        m_file(file),
        m_value(),
        m_valid(false)
    {
        Next();
    }

    ReadIterator& operator++()
    {
        SOFT_CHECK(m_valid);

        Next();

        return *this;
    }

    ReadIterator operator++(int)
    {
        ReadIterator tmp = *this;
        ++*this;
        return tmp;
    }

    /** Получить текущее значение */
    virtual data_type GetValue() const
    {
        SOFT_CHECK(m_valid);

        return m_value;
    }

    /** Валидность итератора */
    operator bool() const
    {
        return m_valid;
    }

    bool operator <(const ReadIterator &it) const
    {
        return GetValue() < it.GetValue();
    }

    bool operator >(const ReadIterator &it) const
    {
        return GetValue() > it.GetValue();
    }

    virtual ~ReadIterator(){}

private:
    void Next()
    {
        m_valid = m_file->Read(m_value);
    }

private:
    t_File m_file; ///< итерируемый файл
    data_type m_value; ///< значение в текущей позиции в файле
    bool m_valid; ///< можно ли пользоваться итератором
};

template<typename T>
std::ostream& operator<<(std::ostream &s, const ReadIterator<T> &it)
{
    s << it.GetValue();
    return s;
}


/** Объединяет несколько отсортированных блоков в один отсортированный файл */
template<typename T>
class FileBlocksMerger
{
    typedef T data_type;
public:
    /**
     * Создать объект-объединителя
     * @param srcSortedFiles отсортированные блоки
     * @param dstFile результирующий отсортированный файл
     */
    FileBlocksMerger(std::vector<t_File> &srcSortedFiles, t_File dstFile) :
        m_srcSortedFiles(srcSortedFiles),
        m_dstFile(dstFile)
    {
    }

    /**
     * Выполнить объединение блоков в один файл
     */
    void MergeBlocksToFile()
    {
        typedef ReadIterator<data_type> file_iterator;
        std::priority_queue<file_iterator, std::vector<file_iterator >, std::greater<file_iterator >  > blocks;

        for (auto file : m_srcSortedFiles)
        {
            file_iterator it(file);
            if (it)
            {
                blocks.push(it);
            }
        }

        while (!blocks.empty())
        {
            auto it = blocks.top();

            blocks.pop();

            m_dstFile->Write(it.GetValue());

            if(++it)
            {
                blocks.push(it);
            }
        }
    }

    virtual ~FileBlocksMerger(){};

private:
    std::vector<t_File> m_srcSortedFiles;
    t_File m_dstFile;
};

#endif /* FILEBLOCKSMERGER_H_ */
