#include "FileSplitter.h"
#include "FileStub.h"

#define CATCH_CONFIG_MAIN
#include "catch.hpp"

using namespace std;

TEST_CASE("FileSplitter", "")
{
    t_FileStub src = make_shared<FileStub>("0123456789");

    t_FileStub dst = make_shared<FileStub>("");

    FileSplitter<char> splitter(src, dst, 7);

    REQUIRE(splitter.SplitOneBlock());
    REQUIRE("0123456" == dst->Str());

    dst->Clear();
    REQUIRE_FALSE(splitter.SplitOneBlock());
    REQUIRE("789" == dst->Str());

    dst->Clear();
    REQUIRE_FALSE(splitter.SplitOneBlock());
    REQUIRE("" == dst->Str());
}
